CC = gcc
CFLAGS = -std=c99 \
-Wall -Wextra -Werror -Wpedantic -Wfloat-equal -Wshadow \
-ggdb3 -Og
ifeq ($(OS), Windows_NT)
    
else
    CFLAGS += `pkg-config --cflags libeditline`
endif

ifeq ($(OS), Windows_NT)

else
    LIBS = `pkg-config --libs libeditline` -lm
endif

lispyy:main.c mpc.o
	$(CC) $(CFLAGS) $(LIBS) -o lispyy main.c mpc.o

.PHONY:clean
clean:
	rm lispyy
	rm *.o
.PHONY:install
install:
	mkdir -p ${out}/bin/.
	cp lispyy ${out}/bin/.

