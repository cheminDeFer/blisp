with import <nixpkgs> {}; rec {
  ebispEnv = stdenv.mkDerivation {
    name = "blisp-env";
    nativeBuildInputs = [pkg-config];
    hardeningDisable = [ "all" ];
    buildInputs = [editline];
    src = ./.;
  };
}
