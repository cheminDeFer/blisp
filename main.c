#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#ifdef _WIN32
#include <string.h>
static char buffer[2048];

char* readline(char* prompt){
	fputs(promt, stdout);
	fgets(buffer, 2048, stdin);
	char* cpy = malloc(strlen(buffer)+1);
	strcpy(cpy,buffer);
	cpy[strlen(cpy) - 1] = '\0';
	return cpy;
}
#else

#include <editline.h>
#endif /* _WIN32 */

#include "mpc.h"
#include <assert.h>

mpc_parser_t* Number;
mpc_parser_t* String;
mpc_parser_t* Comment;
mpc_parser_t* Symbol;
mpc_parser_t* Sexpr;
mpc_parser_t* Qexpr;
mpc_parser_t* Expr;
mpc_parser_t* Lispy;


struct lval;
struct lenv;

typedef struct lval lval;
typedef struct lenv lenv;
typedef struct function Function;

typedef lval* (*lbuiltin)(lenv*, lval*);

typedef void (*deleter)(void*);


typedef struct {
	int owner_c;
	void* res;
	deleter del;
} resource_T ;



void resource_inc_rc(resource_T *r){
	r->owner_c++;	
}

void resource_dec_rc(resource_T *r){
	r->owner_c--;
	if (!r->owner_c) {
		r->del(r->res);
		free(r);
	}
}
resource_T* resource_new(void* res , deleter del){
	resource_T* r =malloc(sizeof(resource_T));
	r->owner_c = 1;
	r->res = res;
	r->del = del;
	return r;	
}


typedef union {
	long num;
	double dnum;
	char* err; 
	char* sym;
	char* str;
	resource_T* r;
}lvalAs;

struct function{
	lbuiltin builtin;
	lenv* env;
	lval* formals;
	lval* body;
};


struct lval
{
	int type;
	int count;
	lvalAs as;
	Function func;
	struct lval** cell;
};
/* TODO: send cell ptr to union as */

enum {
	LVAL_ERR, 
	LVAL_SYM,
	LVAL_FUN, 
	LVAL_SEXPR,/* 3 */
	LVAL_QEXPR,
	LVAL_NUM,
	LVAL_DNUM,
	LVAL_STR,
	LVAL_RES,
};



lval* lval_num(long x)
{
	lval* v = malloc(sizeof(lval));
	if (v == NULL) {
		fprintf(stderr,"%s\n",strerror(errno));
		exit(1);
	}
	v->type = LVAL_NUM;
	v->as.num = x;

	return v;
}

lval* lval_err(char* fmt, ...)
{
	lval* v = malloc(sizeof(lval));
	v->type = LVAL_ERR;

	/* create a va list and initialize*/
	va_list va;
	va_start(va, fmt);
	/* alloc with size */
	v->as.err = malloc(512);

	/* printf with va list*/
	vsnprintf(v->as.err, 511, fmt, va);

	/*re alloc to actual string size*/
	v->as.err = realloc(v->as.err, sizeof(char)*(strlen(v->as.err) + 1));

	/*clean up va list*/
	va_end(va);
  
	return v;
}

lval* lval_sym(char* s)
{
	lval* v = malloc(sizeof(lval));
	v->type = LVAL_SYM;
	v->as.sym = malloc(strlen(s) + 1);
	strcpy(v->as.err, s);
	return v;
}

lval* lval_str(char* s)
{
	lval* v = malloc(sizeof(lval));
	v->type = LVAL_STR;
	v->as.str = malloc(strlen(s) + 1);
	strcpy(v->as.str, s);
	return v;
}

lval* lval_fun(lbuiltin f)
{
	lval *res = malloc(sizeof(lval));
	res->type = LVAL_FUN;
	res->func.builtin = f;
	return res;
}

lenv* lenv_new(void);
lval* lval_lambda(lval* formals, lval* body)
{
	lval *res = malloc(sizeof(lval));
	res->type = LVAL_FUN;
	res->func.builtin = NULL;

	res->func.env = lenv_new();
  
	res->func.formals  = formals;
	res->func.body  = body;
	return res;
}

lval* lval_dnum(double x)
{
	lval* v = malloc(sizeof(lval));
	v->type = LVAL_DNUM;
	v->as.dnum = x;
	return v;
}

lval* lval_sexpr(void)
{
	lval* v = malloc(sizeof(lval));
	v->type = LVAL_SEXPR;
	v->count = 0;
	v->cell = NULL;
	return v;
}

lval* lval_qexpr(void)
{
	lval* v = malloc(sizeof(lval));
	v->type = LVAL_QEXPR;
	v->count = 0;
	v->cell = NULL;
	return v;
}

void fclosew (void *f){
	if( fclose( (FILE*) f) < 0)
		fprintf(stderr, "Error: cannot close file due to %s", strerror(errno));
}

lval* lval_res(void* r, deleter d)
{
	lval* v = malloc(sizeof(lval));
	v->type = LVAL_RES;
	v->as.r = resource_new(r, d);
	v->count = 0;
	v->cell = NULL;
	return v;
}



lenv* lenv_copy(lenv *e);
lval* lval_copy(lval* u)
{
	lval* y = malloc(sizeof(lval));
	y->type = u->type;
	switch (u->type) {
	case LVAL_NUM: 
	case LVAL_DNUM:{
		/* numbers can be copied with only biggest field due to union */
		char dummy[sizeof(double) >= sizeof(lvalAs) ? 1 : -1]; /* static assertion */
		(void)dummy;
		y->as.dnum = u->as.dnum;
		break;
	}
	case LVAL_FUN: {
		if (u->func.builtin) {
			y->func = u->func;
		}
		else{
			y->func.builtin = NULL;
			y->func.env = lenv_copy(u->func.env);
			y->func.formals = lval_copy(u->func.formals);
			y->func.body = lval_copy(u->func.body);
      
		}
		break;
	}
	case LVAL_ERR:
	case LVAL_STR:
	case LVAL_SYM: {
		y->as.sym = malloc(strlen(u->as.sym) + 1);
		strcpy(y->as.sym, u->as.sym);
		break;
	}
	case LVAL_QEXPR:
	case LVAL_SEXPR: {
		y->count = u->count;
		y->cell = malloc(sizeof(lval*) * u->count);
		for (int i = 0; i < u->count; ++i) {
      
			y->cell[i] = lval_copy(u->cell[i]);
		}


		break;
	}
	
	case LVAL_RES: {
		y->as.r = u->as.r;
		resource_inc_rc(y->as.r);
		break;
	}
		

	default:
		assert(0 && "unknown lval type in lval_copy");
		break;
	}
	return y;
}

void lenv_del(lenv *e);
void lval_del(lval* v)
{
	assert(v);
	switch (v->type) {
	case LVAL_NUM: {
		/* numbers have no other allocation */
		break;
	}
	case LVAL_DNUM: {
		/* numbers have no other allocation */
		break;
	}
	case LVAL_FUN: {
    
		/*if not builtin delete other allocs*/
		if (!v->func.builtin) {
      
			lenv_del(v->func.env);
			lval_del(v->func.formals);
			lval_del(v->func.body);
		}
    
    
		break;
	}
	case LVAL_ERR: {
		free(v->as.err);
		break;
	}
	case LVAL_SYM: {
		free(v->as.sym);
		break;
	}
	case LVAL_STR: {
		free(v->as.str);
		break;
	}
	case LVAL_QEXPR:
	case LVAL_SEXPR: {
		int i;
		for (i = 0; i < v->count; ++i) {
			lval_del(v->cell[i]);
		}

		break;
	}
	case LVAL_RES: {
		resource_dec_rc(v->as.r);
		break;
	}
	default:
		fprintf(stderr, "lval type: %d\n", v->type);
		assert(0 && "unknown lval type in del");
		break;
	}
	free(v);
}


struct lenv{
	lenv* par;
	int count;
	char** syms;
	lval** vals;
};

lenv* lenv_new(void)
{
	lenv *e = malloc(sizeof(lenv));
	e->par = NULL;
	e->count = 0;
	e->syms = NULL;
	e->vals = NULL;
	return e;
}
int lval_eq(lval* e1, lval* e2);
int lenv_eq(lenv* e1, lenv* e2)
{
	if (e1 == NULL && e2 == NULL) {
		return 1;
	}
	if (e1->count != e2->count) {
		return 0;
	}
	for (int i = 0; i < e2->count; ++i) {
		if (!(strcmp(e1->syms[i], e2->syms[i]) == 0 ) || !lval_eq(e1->vals[i],e2->vals[i])) {
			return 0;
		}
	}
	if (e1->par != e2->par) {
		return 0;
	}
	return lenv_eq(e1->par, e2->par);

}

lenv* lenv_copy(lenv *e)
{
	lenv *y = malloc(sizeof(lenv));
	y->count = e->count;
	y->par = e->par;
	y->syms = malloc(sizeof(char*) * e->count);
	y->vals = malloc(sizeof(lval*) * e->count);
	for (int i = 0; i < e->count; ++i) {
		y->syms[i] = malloc(strlen(e->syms[i]) + 1);
		strcpy(y->syms[i],e->syms[i]);
		y->vals[i] = lval_copy(e->vals[i]);
	}
	return y;
}

void lenv_del(lenv* e)
{
	for (int i = 0; i < e->count; ++i) {
		free(e->syms[i]);
		lval_del(e->vals[i]);
	}
	free(e->syms);
	free(e->vals);
	free(e);
}

lval* lenv_get(lenv* e, lval* k)
{
	assert(k->type == LVAL_SYM);
	for (int i = 0; i < e->count; ++i) {
		if (strcmp(e->syms[i], k->as.sym) == 0) {
			return lval_copy(e->vals[i]);
		}
	}
	if (e->par ) {
		return lenv_get(e->par,k);
	}

	return lval_err("unbound symbol: %s", k->as.sym);
}

void lenv_put(lenv* e, lval* k, lval* v)
{
	assert(k->type == LVAL_SYM);
	for (int i = 0; i < e->count; ++i) {
		if (strcmp(e->syms[i], k->as.sym) == 0) {
			lval_del(e->vals[i]);
			e->vals[i] = lval_copy(v);
			return;
		}
    
	}
	e->count++;
	e->vals = realloc(e->vals, sizeof(lval*) * e->count);
	e->syms = realloc(e->syms, sizeof(char*) * e->count);
	e->vals[e->count - 1] = lval_copy(v);
	e->syms[e->count - 1] = malloc(strlen(k->as.sym) + 1);
	strcpy(e->syms[e->count - 1], k->as.sym);
  

}

void lenv_def(lenv* e, lval* k, lval* v)
{
	assert(k->type == LVAL_SYM);
	while (e->par) {
		e = e->par;
	}
	lenv_put(e, k, v);
}



lval* lval_read_num(mpc_ast_t* t)
{
	long x;
	errno = 0;
	x = strtol(t->contents, NULL, 10);
	return errno != ERANGE ? lval_num(x) : lval_err("invalid number: %s", t->contents);
}
lval* lval_read_dnum(mpc_ast_t* t)
{
	double x;
	errno = 0;
	x = strtod(t->contents, NULL);
	return errno != ERANGE ? lval_dnum(x) : lval_err("invalid number");
}

lval* lval_add(lval* v, lval *x)
{
	assert(v);
	v->count++;
	v->cell = realloc(v->cell, sizeof(lval*) * v->count);
	if (v->cell == NULL && errno == ENOMEM) {
		fprintf(stderr,"%s\n",strerror(errno));
		exit(23);
  
	}
	v->cell[v->count - 1] = x;
	return v;
}

lval* lval_read_str(mpc_ast_t* t){
	t->contents[strlen(t->contents) - 1] = '\0';
	char* unescaped = malloc(strlen(t->contents + 1) + 1);
	strcpy(unescaped, t->contents + 1);
	
	unescaped = mpcf_unescape(unescaped);
	lval* r = lval_str(unescaped);
	free(unescaped);
	return r;
}

lval* lval_read(mpc_ast_t* t)
{
	lval* x;
	if (strstr(t->tag, "number") && strstr(t->contents, ".")) {
		return lval_read_dnum(t);
	}
	if (strstr(t->tag, "number") && !strstr(t->contents, ".")) {
		return lval_read_num(t);
	}
	if (strstr(t->tag, "symbol")) {
		return lval_sym(t->contents);
	}
	if (strstr(t->tag, "string")) {
		return lval_read_str(t);
	}
	

	x = NULL;
	if (strcmp(t->tag, ">") == 0) {
		x = lval_sexpr();
	}
	if (strstr(t->tag, "sexpr")) {
		x = lval_sexpr();
	}
	if (strstr(t->tag, "qexpr")) {
		x = lval_qexpr();
	}
	
	int i;
	for (i = 0; i < t->children_num; ++i) {
		if (strcmp(t->children[i]->contents, "(") == 0) {
			continue;
		}
		if (strcmp(t->children[i]->contents, ")") == 0 ) {
			continue;
		}
		if (strcmp(t->children[i]->contents, "{") == 0) {
			continue;
		}
		if (strcmp(t->children[i]->contents, "}") == 0 ) {
			continue;
		}
		if (strcmp(t->children[i]->tag, "regex") == 0) {
			continue;
		}
		if (strstr(t->children[i]->tag, "comment")) {
			continue;
		}
		
		x = lval_add(x, lval_read(t->children[i]));
	}
	return x;

}

void lval_print_str(lval *v){
	char *escaped = malloc(strlen(v->as.str) + 1);
	strcpy(escaped, v->as.str);
	escaped = mpcf_escape(escaped);

	printf("\"%s\"", escaped);
	free(escaped);
}

void lval_print(lval *v);
void lval_expr_print(lval* v, char open, char close)
{
	int i;
	assert(v);
	putchar(open);  
	for (i = 0; i < v->count; ++i) {
		lval_print(v->cell[i]);
		if (i != (v->count - 1)) {
			putchar(' ');
		}
    
	}
	putchar(close);
}

void lval_print(lval* v)
{
	assert(v);
	switch (v->type) {
	case LVAL_NUM: {
		printf("%li", v->as.num);
		break;
	}
	case LVAL_DNUM: {
		printf("%g", v->as.dnum);
		break;
	}
	case LVAL_FUN: {
		if (v->func.builtin) {
			printf("<builtin>");
		}else {
			printf("( ");
			lval_print(v->func.formals);
			putchar(' ');
			lval_print(v->func.body);
			putchar(')');
      
      
		}
		break;
	}
	case LVAL_ERR: {
		printf("Error: %s", v->as.err);
		break;
	}
	case LVAL_SYM: {
		printf("%s", v->as.sym);
		break;
	}
	case LVAL_STR:{
		lval_print_str(v);
		break;
	}
	case LVAL_SEXPR: {
		lval_expr_print(v, '(', ')');
		break;
	}
	case LVAL_QEXPR: {
		lval_expr_print(v, '{', '}');
		break;
	}
	case LVAL_RES:{
		printf("Resource at %p ",(void *) v->as.r);
		break;
	}
	default:
		assert(0 && "Unknown lval type in printing");
		break;
	}
}

lval* lval_eval(lenv*e, lval *v);
lval* lval_take(lval *v, int i);
lval* lval_pop(lval* v, int i);
lval* lval_call(lenv* e,lval* f,lval* a);
lval* lval_eval_sexpr(lenv* e, lval* v)
{
	int i;
	/* Eval children*/
	for (i = 0; i < v->count; ++i) {
		v->cell[i] = lval_eval(e,v->cell[i]);
	}
	/* return error if any is error */
	for (i = 0; i < v->count; ++i) {
		if(v->cell[i]->type == LVAL_ERR) {
			return lval_take(v,i);			
		}
	}

	/* empty Expression */
	if (v->count == 0) {
		return v;
	}
	if (v->count == 1) {
		return lval_take(v,0);
	}

	lval *f = lval_pop(v,0);
	
	if (f->type != LVAL_FUN) {
		lval_del(f); lval_del(v);
		return lval_err("First element is not a function!");
	}
	lval* result = lval_call(e, f, v);
	
	lval_del(f);
	return result;

}

int is_formals_contains_x(lval* formals, char *x)
{
	int result = 0;
	assert(formals->type == LVAL_QEXPR);
	for (int i = 0; i < formals->count; ++i) {
		if (strcmp(formals->cell[i]->as.sym,x) == 0) {
			result = 1;
			break;
		}
	}
	return result;
}

lval* lval_eval(lenv* e,lval *v)
{
	if (v->type == LVAL_SYM) {
		lval *x = lenv_get(e,v);
		lval_del(v);
		return x;
	}
	/* Evaluate Sexpressions */
	if (v->type == LVAL_SEXPR) { return lval_eval_sexpr(e,v); }
	/* All other lval types remain the same */
	return v;
}


lval* builtin_eval(lenv* e, lval *a);
lval* builtin_list(lenv* e, lval *a);
lval* lval_call(lenv* e,lval* f,lval* a)
{
	if (f->func.builtin) {
		return f->func.builtin(e,a);
	}
	int given = a->count;
	int total = f->func.formals->count;
  
	if (given > total && !(is_formals_contains_x(f->func.formals, "&"))) {
		lval_del(a);
		return lval_err("Function passed too many parameters. got:%d expected:%d",
				given,total);
	}
  
	while(a->count) {
		lval *sym = lval_pop(f->func.formals,0);
		if (strcmp(sym->as.sym, "&") == 0) {
			lval* nsym = lval_pop(f->func.formals,0);
			lenv_put(f->func.env, nsym, builtin_list(e,a));
			lval_del(sym);
			lval_del(nsym);
			break;
		}
		lval *val = lval_pop(a,0);
		lenv_put(f->func.env, sym , val);
		lval_del(sym); lval_del(val);    
	}
	lval_del(a);
	/*if all bound */
	if (f->func.formals->count == 0) {
		f->func.env->par = e;
		return builtin_eval(f->func.env, lval_add(lval_sexpr(), lval_copy(f->func.body)));
	}else {
		return lval_copy(f);
	}
}



lval * lval_pop(lval* v, int i)
{
	assert(v->count > i);
	assert(v->count >= 0);
	/* find item at "i" */
	lval* x = v->cell[i];

	/* shift memory after i over the top */
	memmove(&v->cell[i], &v->cell[i+1], sizeof(lval*) * (v->count - i - 1));

	/*decrease count of items in list*/
	v->count--;
  
	/*Reallocate*/
	v->cell = realloc(v->cell, sizeof(lval*) * v->count);
	if (v->cell == NULL && errno == ENOMEM) {
		fprintf(stderr,"%s\n",strerror(errno));
		exit(99);
	}

	return x;
}

lval* lval_take(lval* v, int i)
{
	lval* x = lval_pop(v,i);
	lval_del(v);
	return x;
}

static double lval_get_double_num(lval* v)
{
	assert(v->type == LVAL_NUM || v->type == LVAL_DNUM);
	return v->type == LVAL_DNUM ? v->as.dnum : (double) v->as.num;
}
static int lval_get_int_num(lval *v)
{
	assert(v->type == LVAL_NUM || v->type == LVAL_DNUM);
	return v->type == LVAL_NUM ? v->as.num : (int) v->as.dnum;
}  

char* lval_get_type(lval*v)
{
	switch (v->type) {
	case LVAL_NUM: {
		return "number";
		break;
	}
	case LVAL_DNUM: {
		return "dnumber";
		break;
	}
	case LVAL_FUN: {
		return "function";
		break;
	}
	case LVAL_SYM: {
		return "symbol";
		break;
	}
	case LVAL_ERR: {
		return "error";
		break;
	}
	case LVAL_STR: {
		return "string";
	}
	case LVAL_QEXPR: {
		return "Q-Expression";
		break;
	}
	case LVAL_SEXPR: {
		return "S-Expression";
		break;
	}
	default:
		return "unknown type";
		break;
	}
}

lval* builtin_op(lenv*e, lval *a, char* op)
{
  (void) e;
	int i;
	int isAnyD = 0;
  
	for (i = 0; i < a->count; ++i) {
		/* check if any of the operands are not numbers */
		if (a->cell[i]->type != LVAL_NUM  && a->cell[i]->type != LVAL_DNUM ) {
			lval_del(a);
			return lval_err("Cannnot op on non number!");
		}
		/* check if any of the operands are doubles */
		if (a->cell[i]->type == LVAL_DNUM) {
			isAnyD = 1;
		}
	}
	/* first element */
	lval *x = lval_pop(a, 0);

	if ((strcmp(op, "-") == 0) && a->count == 0) {

		if (isAnyD) {
			x->as.dnum = -(x->as.dnum);
		}
		else{
			x->as.num = -(x->as.num);
		}
	}
	if ((strcmp(op,"%") == 0) && a->count > 1) {
		lval_del(x);
		x = lval_err("Div by 0!");
	}
	while(a->count > 0) {
		lval* y =  lval_pop(a,0);
		if (strcmp(op,"+") == 0) {
      
			if (isAnyD) {
				x->as.dnum = lval_get_double_num(x) + lval_get_double_num(y);
				x->type = LVAL_DNUM;
			} else {
				x->as.num += y->as.num;	
			}
		}
		if (strcmp(op,"-") == 0) {
			if (isAnyD) {
				x->as.dnum = lval_get_double_num(x) -  lval_get_double_num(y);
				x->type = LVAL_DNUM;
			} else {
				x->as.num -= y->as.num; 
			}
      
		}
		if (strcmp(op,"*") == 0) {
			if (isAnyD) {
				x->as.dnum = lval_get_double_num(x) *  lval_get_double_num(y);
				x->type = LVAL_DNUM;
			} else {
				x->as.num *= y->as.num; 
			}
      
		}
		if (strcmp(op,"/") == 0) {
			if (y->as.num == 0) {
				lval_del(x);
				lval_del(y);
				x = lval_err("Div by 0!");
				break;
			}
			if (isAnyD) {
				x->as.dnum = lval_get_double_num(x) /  lval_get_double_num(y);
				x->type = LVAL_DNUM;
			} else {
				x->as.num /= y->as.num; 
			}
      
		}
		if (strcmp(op,"^") == 0) {

      
			x->as.dnum = pow(lval_get_double_num(x) , lval_get_double_num(y));
			x->type = LVAL_DNUM;
      
      
		}
		if (strcmp(op,"%") == 0) {
			if (y->as.num == 0) {
				lval_del(x);
				lval_del(y);
				x = lval_err("Div by 0!");
				break;
			}
			x->as.num =  lval_get_int_num(x) % lval_get_int_num(y);
			x->type = LVAL_NUM;
		}
		lval_del(y);

	}
	lval_del(a);
	return x;
  

}

lval* builtin_add(lenv* e, lval*a)
{
	return builtin_op(e, a, "+");
}
lval* builtin_sub(lenv* e, lval*a)
{
	return builtin_op(e, a, "-");
}
lval* builtin_mul(lenv* e, lval*a)
{
	return builtin_op(e, a, "*");
}
lval* builtin_div(lenv* e, lval*a)
{
	return builtin_op(e, a, "/");
}
lval* builtin_modulus(lenv* e, lval*a)
{
	return builtin_op(e, a, "%");
}
void lval_println(lval* a);
lval* builtin_print(lenv* e, lval *a)
{  
	if (a->count != 1) {
		lval_del(a);
		return
			lval_err("print expects %d number of args got :%d",1,a->count);
	}
  
	if (a->type == LVAL_SYM) {
		lval* tmp = lenv_get(e,a);
		lval_del(a);
		lval_println(tmp);
		lval_del(tmp); 
		return lval_sexpr();;
	}  
	lval_println(a->cell[0]);
	lval_del(a);
  
	return lval_sexpr();
}

lval* builtin_error(lenv* e, lval *a)
{
	(void)e;
	if (a->count != 1) {
		lval* err = lval_err("'error' function expects 1 argument. got: %d",
				     a->count);
		lval_del(a);
		return err;
	}
	if (a->cell[0]->type != LVAL_STR) {
		lval* err = lval_err("'error' function expects string type. got: %s",
				     lval_get_type(a->cell[0]));
		lval_del(a);
		return err;
	}

	lval* res = lval_err(a->cell[0]->as.str);
	lval_del(a);
	return res;
}

lval* builtin_load(lenv* e, lval* a){
	
	if (a->count != 1) {
		lval* err = lval_err("'load' function expects 1 argument. got: %d",
				     a->count);
		lval_del(a);
		return err;
	}
	if (a->cell[0]->type != LVAL_STR) {
		lval* err = lval_err("'load' function expects string type. got: %s",
				     lval_get_type(a->cell[0]));
		lval_del(a);
		return err;
	}
	mpc_result_t r;
	
        if (mpc_parse_contents(a->cell[0]->as.str, Lispy, &r)) {

		lval *expr = lval_read(r.output);
		mpc_ast_delete(r.output);
		while(expr->count){
			lval* x = lval_eval(e, lval_pop(expr, 0));
			if (x->type == LVAL_ERR ||
			    (x->type == LVAL_SYM && (strcmp(x->as.sym,"__1") == 0))) {

				lval_del(expr);		
				lval_del(a);
				return x;
				  

			}
			lval_del(x);
		}
		
                
		lval_del(expr);
		
		lval_del(a);

		return lval_sexpr();

		
        } else {
		char* err_msg = mpc_err_string(r.error);
		mpc_err_delete(r.error);
		lval* err = lval_err("Could not load library %s", err_msg);
		free(err_msg);
		lval_del(a);
		return err;
        }
}



static int main_return_val[] = {0};

lval* builtin_exit(lenv* e, lval *a)
{	
	(void)e;
	if (a->count != 1) {
		lval* err = lval_err("exit expected: 1 argument but got :%d.", a->count);
		lval_del(a);
		return err;
	}
	if (a->cell[0]->type != LVAL_NUM) {
		lval* err = lval_err("exit expected: 1st argument type: number"
				    "but got :%s.", lval_get_type(a->cell[0]));
		lval_del(a);
		return err;
	}
	main_return_val[0] = a->cell[0]->as.num & 0xFF;
	lval_del(a);
	return lval_sym("__1");
}


lval* builtin_power(lenv* e, lval*a)
{
	return builtin_op(e, a, "^");
}

lval* builtin_def(lenv* e, lval *a)
{


	if (a->count < 2) {
		lval_del(a);
		return lval_err("def passed wrong number of arguments");
	}
  
	if (a->cell[0]->type != LVAL_QEXPR) {
		lval_del(a);
		return lval_err("def firs argument should be QEXPR");
	}
	lval *syms = lval_pop(a, 0);
	if (syms->count != a->count) {
		lval *r = lval_err("mismatch in number of symbols:%d and values:%d",syms->count ,a->count );
		lval_del(a); lval_del(syms);
		return r;
	}
	for (int i = 0; i < syms->count; ++i) {
		lenv_def(e,syms->cell[i], a->cell[i]);
	}
	lval_del(a); lval_del(syms);
	return lval_sexpr();
  

}

lval* builtin_put(lenv* e, lval *a)
{
	if (a->count < 2) {
		lval_del(a);
		return lval_err("def passed wrong number of arguments");
	}
  
	if (a->cell[0]->type != LVAL_QEXPR) {
		lval_del(a);
		return lval_err("def firs argument should be QEXPR");
	}
	lval *syms = lval_pop(a, 0);
	if (syms->count != a->count) {
		lval *r = lval_err("mismatch in number of symbols:%d and values:%d",syms->count ,a->count );
		lval_del(a); lval_del(syms);
		return r;
	}
	for (int i = 0; i < syms->count; ++i) {
		lenv_put(e,syms->cell[i], a->cell[i]);
	}
	lval_del(a); lval_del(syms);
	return lval_sexpr();
}

lval* builtin_lambda(lenv* e, lval *a)
{
	(void)e;

	if (a->count < 2) {
		lval_del(a);
		return lval_err("lambda passed wrong number of arguments");
	}
  
	if (a->cell[0]->type != LVAL_QEXPR) {
		lval_del(a);
		return lval_err("lambda first argument should be QEXPR");
	}
	if (a->cell[1]->type != LVAL_QEXPR) {
		lval_del(a);
		return lval_err("lambda second argument should be QEXPR");
	}

	for (int i = 0; i < a->cell[0]->count; ++i) {
    
		if ( a->cell[0]->cell[i]->type != LVAL_SYM) {
			lval *r = lval_err("cannot define non symbols got type :%s",lval_get_type(a->cell[0]->cell[i] ));
			lval_del(a); 
			return r;
		}
		int rem_n_of_formals = a->cell[0]->count - 1  - i;
		if (strcmp(a->cell[0]->cell[i]->as.sym, "&") == 0
		    && rem_n_of_formals > 1 ) {
			lval *r = lval_err("after & symbol expected one symbol got: %d",rem_n_of_formals);
			lval_del(a); 
			return r;
		}
	}



	lval* formals = lval_pop(a,0);
	lval* body = lval_take(a,0);
	return lval_lambda(formals, body);
}
lval* lval_cons(lval* a, lval* b);
lval* builtin_fun(lenv* e, lval* a)
{
	(void)e;
	if (a->count != 2) {
		lval* err = lval_err("'fun' expected 2 arg. got:%d", a->count);
		lval_del(a);
		return err;
	}
	if (a->cell[0]->type != LVAL_QEXPR) {
		lval* err = lval_err(
			"'fun' expected 1st arg. type to be:q-expression. got:%s",
			lval_get_type(a->cell[0]));
		lval_del(a);
		return err;
	}
	if (a->cell[1]->type != LVAL_QEXPR) {
		lval* err = lval_err(
			"'fun' expected 2nd arg. type to be: q-expression. got: %s",
			lval_get_type(a->cell[0]));
		lval_del(a);
		return err;
	}
	for (int i = 0; i < a->cell[0]->count; ++i) {
		if (a->cell[0]->cell[i]->type != LVAL_SYM) {
			lval* err = lval_err(
			"'fun' 1st arg. should only contain symbols."
			"Got: %s in: %d",
			lval_get_type(a->cell[0]->cell[i]),i+1);
		lval_del(a);
		return err;
		}
	}

	if (a->cell[0]->count == 0) {
		lval* err = lval_err(
			"'fun' passed {} as 1nd arg.");
		lval_del(a);
		return err;
	}
	
	lval* fname = lval_pop(a->cell[0], 0);
	
	lval* qfname = lval_add(lval_qexpr(),fname);
	/* qfname {sym} */
	qfname = lval_add(lval_qexpr(),qfname);
	/* qfname {{sym}} */
	
	
	lval* lamb = lval_lambda(a->cell[0], a->cell[1]);
        /* qfnamef {{sym} lamb } */
        qfname = lval_add(qfname,lamb);
	return builtin_def(e, qfname);
}

lval* builtin_head(lenv*e, lval* a)
{
	(void)e;
	if (a->count != 1) {
		lval_del(a);
		return lval_err(
			"Function 'head' passed wrong number of arguments."
			"expected: %d passed: %d",
			1, a->count);
	}
  
	if (a->cell[0]->type != LVAL_QEXPR) {
		lval_del(a);
		return lval_err("Function head passed incorrect type");
	}
	if (a->cell[0]->count == 0) {
		lval_del(a);
		return lval_err("Function head passed {}");
	}

	lval *v = lval_take(a,0);
	while(v->count > 1) {lval_del(lval_pop(v,1));}
  
	return v;
}

lval* builtin_tail(lenv*e, lval *a)
{
	(void)e;
	if (a->count != 1) {
		lval_del(a);
		return lval_err("Function tail takes one argument. got:%d",a->count);
	}
	if (a->cell[0]->type !=  LVAL_QEXPR) {
		lval* r = lval_err("Function tail passed incorrect type expected:%s got:%s",
				   "q-expr", lval_get_type(a->cell[0]));
		lval_del(a);
		return r ;
	}
	if (a->cell[0]->count == 0) {
		lval_del(a);
		return lval_err("Function tail passed {}");
	}

	lval* v = lval_take(a, 0);

	lval_del(lval_pop(v,0));
	return v;
}

lval* builtin_last(lenv* e, lval* a){
	(void)e;
	if (a->count != 1) {
		lval_del(a);
		return lval_err("Function last takes one argument. got:%d",a->count);
	}
	if (a->cell[0]->type !=  LVAL_QEXPR) {
		lval* r = lval_err("Function last passed incorrect type."
				   "Expected:%s Got:%s",
				   "q-expr", lval_get_type(a->cell[0]));
		lval_del(a);
		return r ;
	}
	if (a->cell[0]->count == 0) {
		lval_del(a);
		return lval_err("Function tail passed {}");
	}

	lval* v = lval_take(a, 0);
	
	return lval_take(v,v->count-1);

}

lval* builtin_list(lenv* e,lval* a)
{
	(void)e;
	a->type = LVAL_QEXPR;
	return a;
}
lval* builtin_eval(lenv*e,lval *a)
{
	if (a->count != 1) {
		lval_del(a);
		return lval_err("'eval' passed too many arguments!. expected: 1 got: %d",
				a->count);
	}
	if (a->cell[0]->type != LVAL_QEXPR) {
		lval_del(a);
		return lval_err(" 'eval' passed incorrect data type!");
	}
	lval *x = lval_take(a, 0);
	x->type = LVAL_SEXPR;
	return lval_eval(e,x);
}

lval* lval_join(lval *x, lval *y);
lval* builtin_join(lenv *e, lval *a)
{
	(void)e;
	for (int i = 0; i < a->count; ++i) {
		if (a->cell[i]->type != LVAL_QEXPR) {
			lval_del(a);
			return lval_err("join passed incorrect types");
		}
	}  
	lval *x = lval_pop(a,0);
	while(a->count) {
		x = lval_join(x, lval_pop(a,0));
	}
	lval_del(a);
	return x;

}

lval* lval_join(lval *x, lval *y)
{
	while (y->count) {
		x = lval_add(x, lval_pop(y, 0));
	}
	lval_del(y);
	return x;
}

lval* lval_cons(lval* a, lval* b);
lval* builtin_cons(lenv* e,lval* a)
{
	(void)e;
	/*check counter*/
	if (a->count != 2) {
		lval_del(a);
		return lval_err("too many args to 'cons' expected: 2 got: %d", a->count);
	}
	/*check type*/
	if (a->cell[1]->type != LVAL_QEXPR) {    
		lval_del(a);
		return lval_err("second operand must should be QEXPR ");
	}
	lval *x = lval_pop(a,0);
	lval *y = lval_take(a,0);
	return lval_cons(x, y);
}

lval* lval_cons(lval* a, lval* b)
{

  
	/*increase counter*/
	b->count++;
	/*re alloc memory to get one more elem*/
	b->cell = realloc(b->cell,sizeof(lval*) * b->count);  
	/*shift*/
	memmove(&b->cell[1],&b->cell[0],sizeof(lval*) * (b->count - 1));
    
	/* add to front*/
	b->cell[0] = a;
	/*make first input qexpr*/
  
	return b;
}

lval* builtin_len(lenv *e,lval *v)
{
	(void)e;
	/*arg counter check*/
	if (v->count != 1) {
		lval_del(v);
		return lval_err("too many args to 'len' expected '1' ");
	}
	lval *a = lval_take(v,0);
	/* unity when passed error */
	if (a->type == LVAL_ERR) {
		return a;
	}
	/*type check*/
  
	if (a->type != LVAL_SEXPR) {
		lval_del(a);
		return lval_err("operandd must be QEXPR ");
	}
  
	lval* result =lval_num(a->count);
	lval_del(a);
  
	return result;
}

lval* builtin_init(lenv *e, lval *v)
{
	(void) e;
	/*arg counter check*/
	if (v->count != 1) {
		lval_del(v);
		return lval_err("too many args to 'init' expected: 1 got: %d",v->count);
	}
	lval *a = lval_take(v,0);
	/*become unity function if passed error*/
	if (a->type == LVAL_ERR) {
		return a;
	}
	/*type check*/
  
	if (a->type != LVAL_QEXPR) {
		lval_del(a);
		return lval_err("operand must should be QEXPR ");
	}
	if (a->count == 0) {
		lval_del(a);
		return lval_err("function 'init' passed {}");
	}
	lval_del(lval_pop(a,a->count-1));
  
  
	return a;
}

lval* builtin_progn(lenv* e, lval* v)
{
  
	while(v->count) {
		lval* cur =  lval_eval(e, lval_pop(v, 0));
		if (cur->type == LVAL_ERR || v->count == 0) {
			lval_del(v);
			return cur;
		} 
		lval_del(cur);
	}  
	return v;
}

lval* builtin_map(lenv* e, lval* v)
{
	if (v->count != 2) {
		lval* err = lval_err("'map' expected 2 input got: %d", v->count);    
		lval_del(v);
		return err;
	}
	if (v->cell[0]->type != LVAL_FUN) {
		lval* err = lval_err("first argument to map must be function. got: %s",
				     lval_get_type(v->cell[0]));
		lval_del(v);
		return err;
	}
	if (v->cell[1]->type != LVAL_QEXPR) {
		lval* err = lval_err("second argument to map must be qexpr. got: %s",
				     lval_get_type(v->cell[1]));
		lval_del(v);
		return err;
	}
	lval *res = lval_qexpr();
	while (v->cell[1]->count) {
		lval* tobecalld = lval_copy(v->cell[0]);
		lval* args = lval_qexpr();
		args = lval_add(args, lval_pop(v->cell[1], 0));
    
		lval* cur = lval_call(e, tobecalld, args);
		if (cur->type == LVAL_ERR) {
			lval_del(tobecalld);
			lval_del(v);
			return cur;
		}
		lval_add(res,cur);
		lval_del(tobecalld);
	}
	lval_del(v);
	return res;  
}

lval* builtin_ord(lenv* e, lval* v, char* op);
lval* builtin_lt(lenv* e, lval* v)
{
	return builtin_ord(e, v, "<");
}
lval* builtin_gt(lenv* e, lval* v)
{
	return builtin_ord(e, v, ">");
}
lval* builtin_gte(lenv* e, lval* v)
{
	return builtin_ord(e, v, ">=");
}
lval* builtin_lte(lenv* e, lval* v)
{
	return builtin_ord(e, v, "<=");
}

lval* builtin_ord(lenv* e, lval* v, char* op)
{
	
	(void)e;
	if (v->count != 2) {
		lval* err = lval_err("'%s' expected 2 input got: %d", op, v->count);    
		lval_del(v);
		return err;
	}
	if (v->cell[0]->type != LVAL_NUM && v->cell[0]->type != LVAL_DNUM)  {
		lval* err = lval_err("first argument to '%s' must be number. got: %s",
				     op, lval_get_type(v->cell[0]));
		lval_del(v);
		return err;
	}
	if (v->cell[1]->type != LVAL_NUM && v->cell[1]->type != LVAL_DNUM ) {
		lval* err = lval_err("second argument to '%s' must be number. got: %s",
				     op, lval_get_type(v->cell[1]));
		lval_del(v);
		return err;
	}
  
	int r = 0;
	if (strcmp(op, ">") == 0) {
		r = lval_get_double_num(v->cell[0]) > lval_get_double_num(v->cell[1]);
	}
	if (strcmp(op, ">=") == 0) {
		r = lval_get_double_num(v->cell[0]) >= lval_get_double_num(v->cell[1]);
	}
	if (strcmp(op, "<")  == 0) {
		r = lval_get_double_num(v->cell[0]) < lval_get_double_num(v->cell[1]);
	}
	if (strcmp(op, "<=") == 0) {
		r = lval_get_double_num(v->cell[0]) <= lval_get_double_num(v->cell[1]);
	}
	lval_del(v);
	return lval_num(r);
}

int lval_eq(lval* x, lval* y)
{
	if (x->type != y->type) {
		return 0;
	}
	switch (x->type) {
	case LVAL_NUM: {
		return y->as.num == x->as.num;
		break;
	}
	case LVAL_DNUM:{
		double diff = y->as.dnum -  x->as.dnum;
		double abs = diff > 0.0 ? diff : -diff; 
		return abs < 1e-6; /* FIXME: magic number tolerance */
		break;
	}
	case LVAL_FUN: {
		if (x->func.builtin || y->func.builtin) {
			return y->func.builtin == x->func.builtin;
		}
		else{
      
			return lenv_eq(y->func.env,x->func.env) &&
				lval_eq(y->func.formals ,x->func.formals) &&
				lval_eq(y->func.body,x->func.body);
      
		}
		break;
	}
	case LVAL_STR:
	case LVAL_ERR: 
	case LVAL_SYM: {
		return (strcmp(y->as.sym, x->as.sym) == 0);
		break;
	}
	case LVAL_QEXPR:
	case LVAL_SEXPR: {
		if (y->count != x->count) {return 0;}
    
		for (int i = 0; i < x->count; ++i) {
      
			if (!lval_eq(x->cell[i],y->cell[i])) {
				return 0;
			}	
      
		}
		return 0;
		break;
	}

	default:
		assert(0 && "unknown lval type in lval_eq");
		exit(68);
		break;
	}
	return -1;
}
  


lval* builtin_if(lenv *e, lval* v)
{

	if (v->count != 3) {
		lval* err = lval_err("'if' expected 3 input got: %d", v->count);    
		lval_del(v);
		return err;
	}
	if (v->cell[0]->type != LVAL_NUM) {
		lval* err = lval_err("first argument to if must be int number. got: %s",
				     lval_get_type(v->cell[0]));
		lval_del(v);
		return err;
	}
  
	for (int i = 1; i < 3; ++i) {
		if (v->cell[i]->type != LVAL_QEXPR) {
			lval* err = lval_err("two or third argument to if must be qexpr but got: %s in:%d",
					     lval_get_type(v->cell[i]), i + 1);
			lval_del(v);
			return err;
		}
    
	}
	if (v->cell[0]->as.num) {
		lval_del(lval_pop(v, 0)); 
		lval_del(lval_pop(v, 1));    /* when we pop count decreased*/
		v->cell[0]->type = LVAL_SEXPR;
		return lval_eval_sexpr(e, v->cell[0]);
	}else{
		lval_del(lval_pop(v, 0));
		lval_del(lval_pop(v, 0));   /* when we pop count decreased*/
		v->cell[1]->type = LVAL_SEXPR;
		return lval_eval_sexpr(e, v->cell[1]);
	}  
}



lval* builtin_cmp(lenv *e, lval* v, char *op)
{
	(void) e;
	if (v->count != 2) {
		lval* err = lval_err("'%s' expected 2 input got: %d",
				     op, v->count);    
		lval_del(v);
		return err;
	}
	int r = 0;
	if (strcmp(op, "==") == 0) {
		r = lval_eq(v->cell[0], v->cell[1]);
	}
	if (strcmp(op, "!=") == 0) {
		r = !lval_eq(v->cell[0], v->cell[1]);
	}
	lval_del(v);
	return lval_num(r);
}
lval* builtin_eq(lenv *e, lval* v)
{
	return builtin_cmp(e, v, "==");
}
lval* builtin_ne(lenv *e, lval* v)
{
	return builtin_cmp(e, v, "!=");
}

void fclosew(void*);
lval* builtin_open(lenv* e, lval* v){
	(void)e;
	if (v->count != 2) {
		lval* err = lval_err("open expected 2 arguments got: %d", v->count );
		lval_del(v);
		return err;
	}
	for (int i = 0; i < v->count; ++i) {
		if (v->cell[i]->type != LVAL_STR) {
			lval* err = lval_err("open expected 2 arguments to be string "
					     "got: %s in: %d",
					     lval_get_type(v->cell[i]), i + 1);
			lval_del(v);
			return err;
		}
		
	}
	FILE* floc = fopen(v->cell[0]->as.str, v->cell[1]->as.str);
	if (floc == NULL) {
		lval* err = lval_err("%s", strerror(errno));
		lval_del(v);
		return err;
	}
	lval_del(v);
	return lval_res(floc,&fclosew);		
}

lval* builtin_write(lenv* e, lval* v){
	(void)e;	
	if (v->count != 2) {
		lval* err = lval_err("write expected 2 arguments got: %d", v->count );
		lval_del(v);
		return err;
	}
	if (v->cell[0]->type != LVAL_RES || v->cell[0]->as.r->del != &fclosew) {
		lval* err = lval_err("write expected its argument type "
				     "to be file opened via open."
				     "Got: %s in",
				     lval_get_type(v->cell[0]));
		lval_del(v);
		return err;
	}
	if (v->cell[1]->type != LVAL_STR) {
		lval* err = lval_err("write expected second argument type "
				     "to be string got: %s", lval_get_type(v->cell[1]));
		lval_del(v);
		return err;
	}
	lval *str = lval_pop(v, 1);
	char *s = str->as.str;
	
	size_t ret = fwrite(s,
	       strlen(s), sizeof(*s),
	       v->cell[0]->as.r->res);
	if (ret < sizeof(*s)) {
		lval* err = lval_err("cannot write to a file due to: %s", "some err");
		lval_del(str);
		return err;
	}
	lval_del(v);
	lval_del(str);
	return lval_sexpr();
	
	
}

void lenv_add_builtin(lenv* e, char *symbol, lbuiltin func)
{
	lval* k = lval_sym(symbol);
	lval* v = lval_fun(func);
	lenv_put(e, k, v);
	lval_del(k); lval_del(v);
}


void lval_println(lval* v)
{
	lval_print(v); putchar('\n');
}

void lenv_add_builtins(lenv* e)
{
	/* variable functions */
	lenv_add_builtin(e, "def", builtin_def);
	lenv_add_builtin(e, "=", builtin_put);
	lenv_add_builtin(e, "lambda", builtin_lambda);
	lenv_add_builtin(e, "fun", builtin_fun);
	/* list functions */
	lenv_add_builtin(e, "list", builtin_list);
	lenv_add_builtin(e, "head", builtin_head);
	lenv_add_builtin(e, "tail", builtin_tail);
	lenv_add_builtin(e, "last", builtin_last);
	lenv_add_builtin(e, "eval", builtin_eval);
	lenv_add_builtin(e, "join", builtin_join);
	lenv_add_builtin(e, "cons", builtin_cons);
	lenv_add_builtin(e, "len" , builtin_len);
	lenv_add_builtin(e, "init", builtin_init);
	/*utils*/
	lenv_add_builtin(e, "progn", builtin_progn);
	lenv_add_builtin(e, "if", builtin_if);
	lenv_add_builtin(e, "<", builtin_lt);
	lenv_add_builtin(e, "<=", builtin_lte);
	lenv_add_builtin(e, ">", builtin_gt);
	lenv_add_builtin(e, ">=", builtin_gte);
	lenv_add_builtin(e, "==", builtin_eq);
	lenv_add_builtin(e, "!=", builtin_ne);
  
	lenv_add_builtin(e, "map", builtin_map);
  
  
	
	/* math functions */
	lenv_add_builtin(e, "+", builtin_add);
	lenv_add_builtin(e, "-", builtin_sub);
	lenv_add_builtin(e, "*", builtin_mul);
	lenv_add_builtin(e, "/", builtin_div);
	lenv_add_builtin(e, "%", builtin_modulus);
	lenv_add_builtin(e, "^", builtin_power);

	/*dump functions*/
	lenv_add_builtin(e, "print", builtin_print);
	lenv_add_builtin(e, "load", builtin_load);
	lenv_add_builtin(e, "error", builtin_error);
	lenv_add_builtin(e, "open", builtin_open);
	lenv_add_builtin(e, "write", builtin_write);
	/* exit */
	lenv_add_builtin(e, "exit", builtin_exit);
}



int main(int argc, char** argv) {



	/* Create Some Parsers */
	Number   = mpc_new("number");
	String   = mpc_new("string");
	Comment   = mpc_new("comment");
	Symbol = mpc_new("symbol");
	Sexpr = mpc_new("sexpr");
	Qexpr = mpc_new("qexpr");
	Expr     = mpc_new("expr");
	Lispy    = mpc_new("lispy");
 
	/* Define them with the following Language */
	mpca_lang(MPCA_LANG_DEFAULT,
		  "\
	number : /-?[0-9]+\\.?[0-9]*/ ;						\
	string : /\"(\\\\.|[^\"])*\"/ ;						\
	comment : /;[^\\r\\n]*/ ;						\
	symbol : /[a-zA-Z0-9_+\\-*\\/\\\\=<>!&%^]+/ ;				\
	sexpr : '(' <expr>* ')' ;						\
	qexpr : '{' <expr>* '}' ;						\
	expr     : <number> | <symbol> | <sexpr> | <qexpr> | <string> | <comment>;			\
	lispy    : /^/ <expr>* /$/ ;						\
	",
		  Number, String, Comment, Symbol, Sexpr, Qexpr, Expr, Lispy);



	lenv* e = lenv_new();
	lenv_add_builtins(e);

	if (argc >= 2) {
		for (int i = 1; i < argc; ++i) {
			lval* args = lval_add(lval_sexpr(), lval_str(argv[i]));
			lval* res  = builtin_load(e, args);
			if (res->type == LVAL_ERR) {
				lval_println(res);
				break;
				lval_del(res);
			}
			lval_del(res);
		}

	}
	
	/* Print Version and Exit Information */
	if (argc == 1) {
		puts("Lispy Version 0.0.0");
		puts("'C-d'  to Exit\n");
	}
	
	int quit = 0;
	/* In loop */
	while (!quit && argc == 1) {

		/* Output our prompt and get input */
		char* input = readline("lispy> ");

		/* Add input to history */
		add_history(input);
		/* Check if C-d pressed on linux ie EOF*/
		if (input == NULL) {
			break;
		}
		mpc_result_t r;
		if (mpc_parse("<stding>", input, Lispy, &r)) {
			lval *res = lval_eval(e,lval_read(r.output));
			if (res->type == LVAL_SYM &&
			    strcmp(res->as.sym , "__1") == 0) {
				quit = 1;
				lval_del(res);
				mpc_ast_delete(r.output);
				free(input);
				break;
			}
			lval_println(res);

			lval_del(res);
      

			mpc_ast_delete(r.output);
		}else {
			mpc_err_print(r.error);
			mpc_err_delete(r.error);
      
		}
		/* Free retrieved input */
		free(input);

	}
	lenv_del(e);
	mpc_cleanup(8, Number, String, Comment, Symbol, Sexpr, Qexpr, Expr, Lispy);
	
	return main_return_val[0];
}
